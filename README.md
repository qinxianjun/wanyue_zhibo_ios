<div align=center><img src="https://images.gitee.com/uploads/images/2021/0929/144611_f359aba0_8162876.png" width="590" height="212"/></div>

### 项目说明（如果对你有用，请给个star！）
##### <a target="_blank" href="https://www.kancloud.cn/wanyuekeji/wanyue_open_zhibo_ios/2484021">项目文档</a> |  <a target="_blank" href="https://www.kancloud.cn/wanyuekeji/wanyue_open_zhibo_ios/2484022">部署文档</a>  |  <a target="_blank" href="https://www.kancloud.cn/wanyuekeji/wanyue_open_zhibo_ios/2484025">常见问题</a> 

---

### 系统演示
- 总后台地址: <a target="_blank" href="https://malldemo.sdwanyue.com/admin/login/index">https://malldemo.sdwanyue.com/admin/login/index</a> 账号：visitor     密码：visitor
- 商户后台地址: <a target="_blank" href="https://malldemo.sdwanyue.com/shop/index">https://malldemo.sdwanyue.com/shop/index</a> 账号：15711449029 验证码：123456（点击获取验证码后，输入该验证码即可）    


### Android版演示地址（可直接跳转查看）
   - 仓库地址: <a target="_blank" href="https://gitee.com/WanYueKeJi/wanyue_zhibo_android">点击此处</a>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0929/203437_680692e1_8162876.png "155114_9bce1969_8162876.png")
### 项目介绍
万岳科技可为商家快速搭建起一套属于自己的直播商城系统，有效避开商城直播过程中的痛点难点，加入自身创意的同时，汲取各家平台的特色功能和体验，并且可根据用户的运营需求对系统做定制开发。
* 所有使用到的框架或者组件都是基于开源项目,代码保证100%开源。
* 系统功能通用，无论是个人还是企业都可以利用该系统快速搭建一个属于自己的直播商城系统。  

基于ThinkPhp6.0+ios原生开发的一套万岳直播商城系统，专业售后技术团队，让您二开无忧。

### 功能简介
![输入图片说明](https://images.gitee.com/uploads/images/2021/1018/174338_93b518e8_8162876.png "组 1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0928/182630_1b66eb38_8162876.png "组 2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0929/120111_acd0b536_8162876.gif "MAIN (4k)_12.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0929/142722_b321363f_8162876.png "组 3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0929/142737_a290425c_8162876.png "组 4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0929/203456_f7043a89_8162876.png "组 7.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0929/142747_fa3e3a8c_8162876.png "组 5.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0928/183206_39216a74_8162876.png "组 6.png")
 ### 功能介绍表
![输入图片说明](210122_47ed5487_8162876.png)
 ### 开源版使用须知
    
   - 允许用于个人学习、教学案例
    
   - 开源版不适合商用，商用请购买商业版
    
   - 禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负
                
  
### 开源交流群【加群回答请填写“gitee直播”，免费获取sql脚本】

<div style='height: 130px'>
    <img class="kefu_weixin" style="float:left;" src="https://gitee.com/WanYueKeJi/wanyue_education_uniapp/raw/newone/pages/%E5%BC%A0%E7%9A%93%E5%BC%80%E6%BA%90.png" width="602" height="123"/>
    <div style="float:left;">
        <p>QQ：2770722087</p>
        <p>QQ群：995910672</p>
    </div>
</div>
<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=JShAyXeoKqg2lWFEUSElxELImhjeMG4y&jump_from=webapi"><img border="0" src="https://images.gitee.com/uploads/images/2021/0317/100424_072ee536_8543696.png" alt="万岳在线教育讨论群" title="万岳在线教育讨论群"></a>



> QQ群：995910672
 
<img class="kefu_weixin" style="float:left;" src="https://images.gitee.com/uploads/images/2021/0524/181101_c6bda503_2242923.jpeg" width="102" height="102"/>